
# Keil5_disp_size_bar V1.0


## 简介


[Keil5_disp_size_bar]<br>
以进度条百分比来显示keil编译后生成的固件对芯片的内存ram和存储flash的占用情况,<br>
并生成各个源码文件对ram和flash的占比整合排序后的map信息的表格和饼图。<br>

![img_disp_size_bar](https://gitee.com/nikolan/pic-bed/raw/master/kdsb5.png)<br>

![a_xlsx](https://gitee.com/nikolan/pic-bed/raw/master/kdsb6.png)<br>


原理是使用C语言遍历当前目录找到keil工程和编译后生成的map文件<br>
然后读取工程文件和map文件来找到对应关键词内的ram和flash的各类信息<br>
最后把信息整合以进度条字符串直观格式输出，同时把map的数据排序后导出csv和xlsx文件，绘制饼图。<br>

注意:再次编译前先关闭已经使用excel打开的表格，或者先把原来的表格移走，不然excel占用表格文件导致没法输出新表格<br>
注意:再次编译前先关闭已经使用excel打开的表格，或者先把原来的表格移走，不然excel占用表格文件导致没法输出新表格<br>
注意:再次编译前先关闭已经使用excel打开的表格，或者先把原来的表格移走，不然excel占用表格文件导致没法输出新表格<br>

该程序容易和keil的after bulid功能集成，以便嵌入式软件工程师方便知道芯片占用情况，进行裁剪和优化。<br>

这个项目也有一个Qt的版本:https://gitee.com/nikolan/keil_development_assistant<br>
表格和进度条等内容直接在图形化的Qt应用查看<br>
不过这个命令行的版本虽然没法交互，但是更容易集成到项目工程内，不太占空间，且纯C库依赖少，执行更快，各有各的好处。<br>

基本能找到编译后生成的map文件,<br>
就能输出生成的代码对ram和flash的占用大小百分比进度条,和表格数据。<br>
只要能找到工程文件和map文件，keil5环境下通用,对于没有使用分散加载的stm32和各种国产32一般都是可以直接适配的。<br>
程序如果没有输出或输出有错误请查看运行日志 _alog.txt，看程序在哪个步骤断开没有往下执行。<br>
项目内附上一个H7B0工程文件和map文件用于测试。

该项目代码可应用于二次开发，任何数据使用C语言整合信息用链表排序后，以表格方式输出。<br>
例如统计自己源码的某些信息，例如RTOS创建的任务信息或者函数关系等。<br>

## 使用方法：

1. 把程序Keil5_disp_size_bar.exe放到工程目录下,<br>
要放在在.map文件更上一层的目录,<br>
例如可以放在和工程文件.uvoptx同一目录下。<br>

![img_dir](https://img.anfulai.cn/dz/attachment/forum/202307/05/120244vxg770io5p70i2f7.png)
<br>

2. 在工程打开魔术棒配置，在User的After Build/Rebuild下<br>
添加编译后执行程序#Run1或#Run2,<br>
在前面打钩，后面则选择要执行的程序Keil5_disp_size_bar.exe对应路径<br>
注意要选对是当前工程目录下的Keil5_disp_size_bar.exe<br>
最好还是使用相对路径，避免工程移动后导致找不到。<br>
（为避免移动工程后，路径找不到的情况。也可手动输入 相对当前工程文件的相对路径，例如："./Keil5_disp_size_bar.exe"）<br>

如果选错其他工程的，编译出的信息则是其他工程的map文件<br>

最新版本支持传入参数<br>
./Keil5_disp_size_bar.exe 参数1 参数2 参数3 参数4 参数5<br>

参数1:工程文件和map文件的递归查找目录，默认是"./"即exe所在目录<br>
参数2:map文件的递归查找目录，默认是"./"即exe所在目录<br>
参数3:是否输出程序详细运行过程，默认是0不输出,1则输出。<br>
参数4:打印进度条已占用时的字符串，默认是"■"<br>
参数4:打印进度条未占用时的字符串，默认是"_"<br>


![img_after_bild](https://gitee.com/nikolan/pic-bed/raw/master/kdsb7.png)
<br>

1. 接着每次对工程按下编译，编译完成后就能看到生成的代码对ram和flash的占用大小百分比进度条。<br>
![img_disp_size_bar](https://gitee.com/nikolan/pic-bed/raw/master/kdsb5.png)
<br>


同时生成了4个文件<br>
- xxx_alog.txt 是程序运行日志文件，当输出不对或没有输出时可以查看<br>
- xxx_sort_by_flash.csv是把文件按flash占用排序的表格<br>
- xxx_sort_by_ram.csv是把文件按ram占用排序的表格<br>
- xxx_analysis.xlsx是文件ram和flash的占用和绘制的百分比饼图<br>
  
![chart](https://gitee.com/nikolan/pic-bed/raw/master/kdsb1.png)<br>
![csv](https://gitee.com/nikolan/pic-bed/raw/master/kdsb2.png)<br>
![alog](https://gitee.com/nikolan/pic-bed/raw/master/kdsb3.png)<br>
![xlsx](https://gitee.com/nikolan/pic-bed/raw/master/kdsb4.png)<br>

1. 如果执行了Keil5_disp_size_bar.exe，却没有输出占用百分比进度条，或者输出的信息有误<br>
请检测输出的错误消息或alog日志文件,最大可能是当前工程或者你放置程序的目录下递归查找也找不到map文件，<br>
请检测工程的Output输出生成配置，然后按下全部重新编译一次再看看能不能输出占用百分比进度条。<br>

![img_find_map](https://gitee.com/nikolan/pic-bed/raw/master/kdsb3.png)<br>

## 二次开发环境搭建

1. 系统:windos10<br>
2. 编译器mingw-64<br>
3. 编辑器:例如vscode(使用其他IDE环境也可)<br>

工程结构如下:<br>
├─.vscode<br>
├─inc(头文件)<br>
│  └─xlsxwriter(头文件)<br>
│      └─third_party(头文件)<br>
├─lib(静态库)<br>
└─src(源码)<br>

开发方式和linux下编译开发C语言差不多,通过make或者gcc指令编译。<br>

源码是使用GB2312格式编码的字符，使用其他编码打开的话中文注释和特殊字符会乱码。
因为keil如果使用utf-8输出到cmd的会带乱码，这是因为window的cmd中文默认GB2312。

代码通过预编译实现了库的可裁剪<br>
Makefile的USED_XLSXWRITER_LIB=1  默认是1，编译xlsxwriter库<br>

如果USED_XLSXWRITER_LIB=0<br>
则代码生成xlsx文件部分代码则被裁剪掉，只会生成csv文件。<br>


   




## 版本更新日志
. 更新到v1.0
版本1.0进行了大更新

- 代码规范化，大部分变量以结构体形式封装，且函数接口有较详细注释
- 支持main函数传参 参数1:工程和map的递归查找地址 参数2：map递归查找地址 参数3是否输出运行过程到终端，参数4进度条占用字符串 参数5进度条不占用字符串
- 支持显示ram和flash段的基地址，且按基地址纠正是ram还是flash
- 程序运行后直接输出结果，程序的运行过程默认不输出到终端而是保存在 工程名_alog.txt内，要输出则传参数4为 1
- 解析文件在已使用的ram和flash的占比，导出csv和xlsx表格结果，xlsx表格会画出百分比的饼图，csv则需要自己使用excel画



. 更新到v0.4
- 把uint64_t改为uint32_t,因为发现sscanf函数的%x给赋值时，64位好像会因为对齐问题错误。
- 修复递归查找不彻底导致子目录下的map找不到
  
- 添加C51支持8051 测试STC89C52和WCH552均测试可以，但是部分工程没有写xram的大小的默认只能以iram大小替代。
还有部分工程是没有在工程定义真正大小的，也就是芯片本身没有keil开发包，用其他芯片或通用开发包定义的工程。
在keil上是没有定义对应芯片型号或用了其他芯片的定义的8051这就会导致显示占用的最大值错误。
要确保工程文件<CPU>里面的IRAM,XRAM,IROM都是正确的才行。

- 同时发现部分例如stm32F0系列的工程map文件格式不同，可能早期map不怎么统一规范，给的max都是0xFFFFFFFF,无法输出进度条，只能读工程的里芯片定义的max，来替代map文件的max，所以部分自定义的显示不出来，工程文件有些格式也是会不一样有的以逗号分隔有的以-分隔。

. 更新到v0.3
- 修改进度条部分字符对齐问题,方块字符选择了正方形等宽
- 根据网友的反馈由于关键词(Exec关键词有部分map文件不存在，关键词改成Execution Region检索执行段
- 加大ram和flash的占用信息存储数组的上限为30，有的map的flash分开的段比较多，或者自定义了.bss的内存池
- 根据网友反馈使用了自定义malloc的内存池的ram被归类为了flash，除了带RAM外添加带ER$$的也视为RAM
- 百分比修改以KB单位的显示占用分子分母
- 最后以B为单位显示剩余可用空间
- %d显示改为%u显示无符号整数
  
. 更新到V0.2
- 更改进度条样式
- 采用关键词(Exec模糊检索ram和flash的size和max
- 支持多个ram和flash的占用百分比进度条显示


-  一个极客 ageek nikola 开源



